#define CATCH_CONFIG_RUNNER
#include "catchorg/catch/catch.hpp"

int main(int argc, char* argv[]) {
	// global setup...

if 	(int result = Catch::Session().run(argc, argv); result)
	return result;
	// global clean-up...
	return 0;
}