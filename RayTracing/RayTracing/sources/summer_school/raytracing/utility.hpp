#pragma once

#include <sstream>
#include <stdexcept>
#include <thread>

namespace summer_school::raytracing::utility {

	template<typename... Arguments>
	auto format(Arguments&& ... arguments) {
		std::stringstream buffer;
		(buffer << ... << std::forward<Arguments>(arguments));
		return buffer.str();
	}

}

#define THROW(exception, ...)                                                                                          \
    throw exception{summer_school::raytracing::utility::format(                                                        \
        "'", #exception, "' exception in thread '", std::this_thread::get_id(),                                        \
        "': ", summer_school::raytracing::utility::format(__VA_ARGS__), "\n    at ", __func__, "(", __FILE__, ":",     \
        __LINE__, ")")};
