#pragma once

#include <cstdint>

namespace summer_school::raytracing::color {

	enum class Colors : uint32_t;
	using channel_type = uint8_t;
	class Color 
	{

	public:
		static constexpr uint32_t kAlphaExtractValue = 0xFF;
		static constexpr uint32_t kShiftValue = 8;
	public:
		/* Constructors */
		constexpr Color(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha = 255) : 
			red_{ red },
			green_{ green },
			blue_{ blue }, 
			alpha_{ alpha } 
		{ /* Empty */ }

		Color() = default;


		explicit Color(uint32_t code) :
			alpha_(code & kAlphaExtractValue),
			blue_((code >>= kShiftValue)& kAlphaExtractValue), 
			green_((code >>= kShiftValue)& kAlphaExtractValue), 
			red_((code >>= kShiftValue)& kAlphaExtractValue)
		{/* Empty */ }

		Color(Colors code) : Color(static_cast<uint32_t>(code)) { /* Empty */ }


		/* Getters */

		auto getCode() const { return (red_ << 24) | (green_ << 16) | (blue_ << 8) | alpha_; }

		auto getRed() const { return this->red_; }

		auto getGreen() const {	return this->green_; }

		auto getBlue() const { return this->blue_; }
		
		auto getAlpha() const { return this->alpha_;}

		/* Operators */

		
		friend bool operator == (const Color& first, const Color& second)
		{
			return first.getCode() == second.getCode();
		}

		

/*
		bool operator == (const Color& color)
		{
			return this->getCode() == color.getCode();
		}
*/
	private:
		uint8_t alpha_;
		uint8_t blue_;
		uint8_t green_;
		uint8_t red_;

	};

	enum class Colors : uint32_t
	{
		Black  =  0x000000FF,
		Red	   =  0xFF0000FF,
		Green  =  0x00FF00FF,
		Blue   =  0x0000FFFF,
		Purple =  0xFF00FFFF,
		Cyan   =  0x00FFFFFF,
		Yellow =  0xFFFF00FF, 
		White  =  0xFFFFFFFF,
		BrightGreen =  0x93F542FF,
		aero = 0x7CB9E8FFu,
		citrine = 0xE4D00AFFu,
		magenta = 0xFF00FFFFu,

	};
}

		//Magenta = 0xD442F5FF,
// 0x79 , 7*16^1 _+ 9 *16^0