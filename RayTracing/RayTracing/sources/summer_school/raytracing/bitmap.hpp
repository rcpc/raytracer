#pragma once

#include "summer_school/raytracing/image.hpp"

#include <string>

namespace summer_school::raytracing::bitmap {

	auto readBitmapFile(const std::string& path)->summer_school::raytracing::image::Image;

	auto writeBitmapFile(const std::string& path, const summer_school::raytracing::image::Image& image) -> void;

}
