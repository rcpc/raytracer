#pragma once

#include "summer_school/raytracing/color.hpp"

#include <algorithm>
#include <stdexcept>
#include <vector>

using summer_school::raytracing::color::Color;
using summer_school::raytracing::color::Colors;


namespace summer_school::raytracing::image {

	class Image
	{
	public:
		Image() = default;

		Image(const std::vector<Color>& myImage) : pixels_(myImage) { /* Empty */ }

		Image(int width, int height) : width_(width), height_(height), pixels_(width_ * height_, Colors::Black) { /* Empty */ }

		friend bool operator == (const Image& first, const Image& second) { return first.pixels_ == second.pixels_; }
		
		auto getColor(int x, int y) const -> Color { return pixels_.at(getIndex(x, y)); }		

		auto plot(int x, int y, Color color) {  pixels_.at(getIndex(x, y)) = color; }

		auto getWidth() const -> int { return  this->width_; }

		auto getHeight() const -> int { return this->height_; }

		int getIndex(int x, int y) const { return y * this->width_ + x; }


	private:
		int width_;
		int height_;
		std::vector<Color> pixels_;
	};
}

