#pragma once

#include "catchorg/catch/catch.hpp"
#include "summer_school/raytracing/vector.hpp"

using Catch::Detail::Approx;
using summer_school::raytracing::vector::Vector3;
using summer_school::raytracing::vector::close;
using summer_school::raytracing::vector::dotProduct;

TEST_CASE("Vector3")
{
	SECTION("Default Constructor")
	{
		auto vector = Vector3();

		REQUIRE(vector.getX() == Approx(0.0));
		REQUIRE(vector.getY() == Approx(0.0));
		REQUIRE(vector.getZ() == Approx(0.0));
	}

	SECTION("Custom Constructor")
	{
		auto vector = Vector3(1, 2, 3);

		REQUIRE(vector.getX() == Approx(1.0));
		REQUIRE(vector.getY() == Approx(2.0));
		REQUIRE(vector.getZ() == Approx(3.0));
	}

	SECTION("Impalce operations")
	{
		auto a = Vector3(1, 2, 3);
		auto b = Vector3(2, 1, 0);

		SECTION("Implace Addition")
		{
			a += b;

			REQUIRE(a.getX() == Approx(3.0));
			REQUIRE(a.getY() == Approx(3.0));
			REQUIRE(a.getZ() == Approx(3.0));
		}

		SECTION("Implace Subtraction")
		{
			REQUIRE(close(a -= b, { -1.0, 1.0, 3.0 }, 1e-15));
			// NOT WORKING WHY? : REQUIRE(a == Vector3(-1, 1, 3));
		}

		SECTION("Implace multiplication")
		{
			REQUIRE(close(a *= 2.0, { 2.0, 4.0, 6.0 }, 1e-15));
			//REQUIRE( (a == Vector3(1,2,3) ));
		}

		SECTION("Operator == ")
		{
			REQUIRE((a += b) == Vector3(3, 3, 3));
		}

		SECTION("dotProduct")
		{
			REQUIRE(dotProduct(a, b) == Approx(4.0));
		}
	}

	SECTION("Binary operations")
	{
		const auto a = Vector3{ 1.0, 2.0, 3.0 };
		const auto b = Vector3{ 10.0, 20.0, 30.0 };

		SECTION("dot product")
		{
			REQUIRE(dotProduct(a, b) == Approx(140.0));
		}

		SECTION("addition")
		{
			REQUIRE(close(a + b, { 11.0, 22.0, 33.0 }));
		}

		SECTION("subtraction")
		{
			REQUIRE(close(a - b, { -9.0, -18.0, -27.0 }));
		}

		SECTION("sclar multiplicaiton")
		{
			REQUIRE(close(a * 3.0, { 3.0, 6.0, 9.0 }));
		}
	}

	SECTION("magnitude")
	{
		REQUIRE(Vector3{ 3.0, 4.0, 5.0 }.getMagnitude() == Approx(7.071));
	}

	SECTION("normalized")
	{
		REQUIRE(true);
		//REQUIRE(close(normalized(Vector3{ 3.0, 4.0, 5.0 }), { 0.424, 0.565, 0.707 }));
	}
}

