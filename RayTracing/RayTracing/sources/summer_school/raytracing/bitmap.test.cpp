#include "catchorg/catch/catch.hpp"
#include "summer_school/raytracing/bitmap.hpp"
#include "summer_school/raytracing/color.hpp"
#include "summer_school/raytracing/image.hpp"

using summer_school::raytracing::bitmap::readBitmapFile;
using summer_school::raytracing::bitmap::writeBitmapFile;
using summer_school::raytracing::color::Colors;
using summer_school::raytracing::image::Image;

TEST_CASE("Bitmap") {
	constexpr auto sqaureSize = 5;
	constexpr auto squareCountX = 5;
	constexpr auto squareCountY = 6;
	auto chessboard = Image{ sqaureSize * squareCountX, sqaureSize * squareCountY };
	for (auto y = 0; y < chessboard.getHeight(); ++y)
		for (auto x = 0; x < chessboard.getWidth(); ++x)
			chessboard.plot(x, y, (x / sqaureSize + y / sqaureSize) % 2 == 0 ? Colors::aero : Colors::magenta);
	writeBitmapFile("actual_chessboard.bmp", chessboard);

	auto expected = readBitmapFile("expected_chessboard.bmp");

	REQUIRE(readBitmapFile("actual_chessboard.bmp") == expected);

	REQUIRE(chessboard == expected);
}


TEST_CASE("Bitmap custom drawing")
{
	auto size = 500;
	auto custom = Image(500, 500);
	writeBitmapFile("actual_chessboard.bmp", custom);

	for (auto x = 0; x < custom.getWidth(); ++x)
	{
		for (auto y = 0; y < custom.getHeight(); ++y)
		{
			if (x % 250 == 0)
				custom.plot(x, y, Colors::Blue);
			else
				custom.plot(x, y, Colors::Cyan);
		}
	}
	writeBitmapFile("actual_chessboard.bmp", custom);
}

